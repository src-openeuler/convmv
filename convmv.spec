Name:          convmv
Version:       2.06
Release:       1
Summary:       Converts filenames from one encoding to another
License:       GPL-2.0-only OR GPL-3.0-only
URL:           http://j3e.de/linux/%{name}
Source0:       http://j3e.de/linux/%{name}/%{name}-%{version}.tar.gz
BuildArch:     noarch
BuildRequires: perl-generators
BuildRequires: perl(Cwd)
BuildRequires: perl(Encode)
BuildRequires: perl(File::Basename)
BuildRequires: perl(File::Compare)
BuildRequires: perl(File::Find)
BuildRequires: perl(Getopt::Long)
BuildRequires: perl(Unicode::Normalize)
BuildRequires: perl(bytes)
BuildRequires: perl(utf8)

%description
convmv converts filenames, directories, and even whole filesystems to a different encoding.
It just converts the filenames, not the content of the files.
A special feature of convmv is that it also takes care of symlinks,
also converts the symlink target pointer in case the symlink target is being converted.

%prep
%autosetup -n %{name}-%{version} -p1
tar -xf testsuite.tar

%build
%make_build

%check
make test

%install
make install PREFIX=%{_prefix} DESTDIR=%{buildroot}

%files
%doc CREDITS Changes TODO
%license GPL2
%{_bindir}/convmv
%{_mandir}/man*/*

%changelog
* Mon Mar 03 2025 Funda Wang <fundawang@yeah.net> - 2.06-1
- update to 2.06

* Thu Nov 28 2019 chenzhenyu <chenzhenyu13@huawei.com> - 2.05-5
- Package init
